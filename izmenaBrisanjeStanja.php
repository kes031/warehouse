<?php include 'konekcija.php';

if($_SESSION['user'] == ''){
  header("Location:login.php");
  exit;
}


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zalihe proizvoda Beograd </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="favicon.ico" rel="shortcut icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">
  <link href="css/datatables.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="preloader"></div>

  <?php include 'header.php'; ?>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Pregled stanja</h3>
          <div class="section-title-divider"></div>
        </div>
      </div>
    </div>
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">

          <table id="tabela" class="table table-hover">
            <thead>
              <tr>
                <th>Prodavnica</th>
                <th>Naziv proizvoda</th>
                <th>Kategorija</th>
                <th>Kolicina</th>
                <th>Izmeni</th>
                <th>Obrisi</th>
              </tr>
            </thead>
            <tbody>
              <?php $stanje = $db->rawQuery("select * from stanje s join prodavnica p on s.prodavnicaID=p.prodavnicaID join proizvod pr on s.proizvodID = pr.proizvodID join kategorija k on pr.kategorijaID=k.kategorijaID");

                  foreach($stanje as $s){
               ?>
               <tr <?php
                  $kol = $s['kolicina'];
                  $mkol = $s['minimalnaKolicina'];
                  $okol = $s['optimalnaKolicina'];
                  if($kol < $mkol){
                    echo 'class="crveno"';
                  }
                  if($kol > $okol){
                    echo 'class="zeleno"';
                  }
                ?>>
                 <td><?php echo $s['nazivProdavnice']; ?> </td>
                 <td><?php echo $s['naziv']; ?> </td>
                 <td><?php echo $s['nazivKategorije']; ?> </td>
                 <td ><?php echo $s['kolicina']; ?> </td>
                 <td ><a href="izmeni.php?id=<?php echo $s['stanjeID']; ?>"><i class="fa fa-refresh fa" aria-hidden="true"></i></a> </td>
                 <td ><a href="obrisi.php?id=<?php echo $s['stanjeID']; ?>"><i class="fa fa-close fa" aria-hidden="true"></i></a> </td>
               </tr>

             <?php  } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="container">
      <form method="post" action="upload.php" enctype="multipart/form-data">
        <div class="form-group">
          <label for="file" class="cols-sm-2 control-label">Dokument</label>
            <div class="cols-sm-10">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-magic fa" aria-hidden="true"></i></span>
                <input type="file" class="form-control" name="file" placeholder="Ubacite fajl sa zahtevima"/>
              </div>
            </div>
        </div>
        <div class="form-group ">
          <input type="submit" name="file" class="btn btn-danger btn-lg " value="Ubaci fajl">
        </div>
      </form>
    </div>
    <div class="container">
      <div id="chart_div"></div>
    </div>
  </section>


  <?php include 'footer.php'; ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>
  <script src="js/datatables.js"></script>
  <script src="js/custom.js"></script>
  <script>
  $(document).ready(function(){
    $('#tabela').DataTable();
    });
  </script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(grafik);

      function grafik() {
    		    var jsonData = $.ajax({
    		    url: "podaciGrafik.php",
    		    dataType:"json",
    		    async: false
    		  }).responseText;
    		  var data = new google.visualization.DataTable(jsonData);
    		  var options = {'title':'Kolicina po proizvodu',
    		   backgroundColor: { fill:'transparent' },
    		    titleTextStyle: {
    		  textAlign: 'center',
    		      color: 'black',
    		      fontSize: 36},
    		      'width':1200,
    		      'height':800,
    		      is3D:true,
    		  legend: {
    		      textStyle: {
    		    color: 'black'
    		      }
    		  },
    		  };

    		  var chart = new google.visualization.PieChart(document.getElementById('chart_div'));


    		  chart.draw(data,  options);

    		}
    </script>
</body>
</html>
