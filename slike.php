<?php include 'konekcija.php';

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zalihe proizvoda Beograd </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="favicon.ico" rel="shortcut icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="preloader"></div>

  <?php include 'header.php'; ?>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Proizvodi slike</h3>
          <div class="section-title-divider"></div>
        </div>
      </div>
    </div>
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <label for="tag">Proizvođači</label>
          <input type="text" id="tag" class="form-control" placeholder="Ime proizvođača">
          <label for="dugme">Pretrazi</label>
          <button  id="dugme" class="form-control btn-primary" onclick="pretragaPoProizvodjacu()">Pretrazi</button>
          <h1>Slike proizvođača</h1>
          <div id="slike"> </div>

        </div>
      </div>
    </div>
  </section>


  <?php include 'footer.php'; ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>

  <script src="js/custom.js"></script>
  <script>
  function pretragaPoProizvodjacu(){
      $("#slike").html("");
    var tag = $("#tag").val();
    $.getJSON( "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?", {
      tags: tag,
      tagmode: "any",
      format: "json"
    })
      .done(function( data ) {
        var output = "";
        $.each( data.items, function( i, item ) {
            output += "<div class='col-md-3'>";
            output +='<img src="'+item.media.m+'" class="img img-responsive">';
            output += '</div>'
        });
        $("#slike").html(output);
      });

  }

    </script>


</body>
</html>
