<?php
include 'konekcija.php';
if($_SESSION['user'] == ''){
  header("Location:login.php");
  exit;
}
$poruka = '';
if(isset($_POST["unesi"])){

    include("domen/proizvodKlasa.php");
    $pr = new Proizvod($db);

    if($pr->unesiProizvod()){
      header("Location: index.php");
    }else{
      $poruka = 'Greska pri dodavanju proizvoda';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zalihe proizvoda Beograd </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="favicon.ico" rel="shortcut icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="preloader"></div>

  <?php include 'header.php'; ?>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Dodavanje proizvoda </h3>
          <div class="section-title-divider"></div>
        </div>
      </div>
    </div>
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="">

            <p><?php
                echo($poruka);
            ?></p>
            <div class="form-group">
              <label for="kategorija" class="cols-sm-2 control-label">Kategorija</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                  <select name="kategorija" class="form-control">
                    <?php
                    $kat = $db->get('kategorija');
                        foreach($kat as $k){
                     ?>
                     <option value="<?php echo $k['kategorijaID'] ;?>"><?php echo $k['nazivKategorije'] ;?></option>

                   <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="naziv" class="cols-sm-2 control-label">Naziv</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-ticket fa" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="naziv" id="naziv"  placeholder="Naziv"/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="opis" class="cols-sm-2 control-label">Opis</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-list fa" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="opis" id="opis"  placeholder="Opis"/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="proizvodjac" class="cols-sm-2 control-label">Proizvodjac</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-tags fa" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="proizvodjac" id="proizvodjac"  placeholder="Proizvodjac"/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="cena" class="cols-sm-2 control-label">Cena</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calculator fa" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="cena" id="cena"  placeholder="Cena"/>
                </div>
              </div>
            </div>


            <div class="form-group ">
              <button type="submit" name="unesi" id="button" class="btn btn-primary btn-lg btn-block">Dodaj proizvod</button>
            </div>

          </form>

        </div>
      </div>
    </div>
  </section>


  <?php include 'footer.php'; ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>

  <script src="js/custom.js"></script>


</body>
</html>
