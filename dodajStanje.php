<?php
include 'konekcija.php';

if($_SESSION['user'] == ''){
  header("Location:login.php");
  exit;
}
$poruka = '';
if(isset($_POST["unesi"])){

    include("domen/stanjeKlasa.php");
    $stanje = new Stanje($db);

    if($stanje->unesiStanje()){
      $poruka = 'Uspesno dodato na stanje';
    }else{
      $poruka = 'Greska pri dodavanju';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zalihe proizvoda Beograd </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="favicon.ico" rel="shortcut icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="preloader"></div>

  <?php include 'header.php'; ?>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Dodavanje proizvoda na stanje</h3>
          <div class="section-title-divider"></div>
        </div>
      </div>
    </div>
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="">

            <p><?php
                echo($poruka);
            ?></p>
            <div class="form-group">
              <label for="prodavnica" class="cols-sm-2 control-label">Prodavnica</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                  <select name="prodavnica" class="form-control">
                    <?php
                    $prod = $db->get('prodavnica');
                        foreach($prod as $p){
                     ?>
                     <option value="<?php echo $p['prodavnicaID'] ;?>"><?php echo $p['nazivProdavnice'] ;?></option>

                   <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="proizvod" class="cols-sm-2 control-label">Proizvod</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-check fa" aria-hidden="true"></i></span>
                  <select name="proizvod" class="form-control">
                    <?php
                    $pro = $db->get('proizvod');
                        foreach($pro as $pr){
                     ?>
                     <option value="<?php echo $pr['proizvodID'] ;?>"><?php echo $pr['naziv'] ;?></option>

                   <?php } ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="kolicina" class="cols-sm-2 control-label">Kolicina</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calculator fa" aria-hidden="true"></i></span>
                  <input type="number" class="form-control" name="kolicina" id="kolicina"  placeholder="Kolicina"/>
                </div>
              </div>
            </div>



            <div class="form-group ">
              <button type="submit" name="unesi" id="button" class="btn btn-primary btn-lg btn-block">Dodaj na stanje</button>
            </div>

          </form>

        </div>
      </div>
    </div>
  </section>


  <?php include 'footer.php'; ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>

  <script src="js/custom.js"></script>


</body>
</html>
